%TO DO:
%{
- nafitovat body na modely z fullBodySkin_baseFrame
    - přečíst si, jak to funguje, v přiloženém pdfku
- vymyslet optimalizační úlohu (Jak spárovat body z modelu s trojúhelníky?)
- nápad schválen => najít osy a krajní body
- Obtáhnout krajní body u modelů z fullBodySkin_baseFrame a modely z
rekonstrukce nafitovat na obrys
%}

%nápad: optimalizační metodou vytipovat body odpovídající taxelům

%návody:
%{
matlab manual for plots: https://www.mathworks.com/help/matlab/ref/plot3.html
matlab manual for figures: https://www.mathworks.com/help/matlab/ref/figure.html
%}

%Takhle se scaluje:
points1=[0 0 0 0 1 1 1 1;
    0 0 1 1 0 0 1 1;
    0 1 0 1 0 1 0 1];
original=figure();
plot3(points1(1,:),points1(2,:),points1(3,:),'.k','Color','blue');
hold on;
points2=diag([2;2;2])*points1;%vynásobit diagonální maticí
plot3(points2(1,:),points2(2,:),points2(3,:),'.k','Color','red');