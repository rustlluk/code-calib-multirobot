function tr_centers = centers_from_edges(edges)
tr_centers=[[0;0;0]];
for i=1:size(edges,2)/3
    tr_centers(:,i)=(edges(:,3*i)+edges(:,3*i-1)+edges(:,3*i-2))*(1/3);
end