for part={'rightArm','leftArm'} %
    part=part{1};
    for i=1:3
        load(strcat(part,num2str(i),'_scaled.mat'));
    end
    
    if(strcmp(part,'leftArm'))
        taxels_leftArm3(1:3,:)=taxels_leftArm3(1:3,:)-taxels_leftArm1(1:3,find(taxels_leftArm1(4,:) == 3));
        taxels_leftArm1(1:3,:)=taxels_leftArm1(1:3,:)-taxels_leftArm1(1:3,find(taxels_leftArm1(4,:) == 3));
        
        z=taxels_leftArm1(1:3,find(taxels_leftArm1(4,:) == 0));
        y=taxels_leftArm1(1:3,find(taxels_leftArm1(4,:) == 2));
        x=cross(y,z);
        z=cross(x,y);
        x=x/norm(x);
        y=y/norm(y);
        z=z/norm(z);
        A=[x y z];
        taxels_leftArm1(1:3,:)=inv(A)*taxels_leftArm1(1:3,:);
        taxels_leftArm3(1:3,:)=inv(A)*taxels_leftArm3(1:3,:);
        
        R=[1 0 0; 0 cos(pi/45) -sin(pi/45); 0 sin(pi/45) cos(pi/45)];
        %Rz=[cos(-pi/90) -sin(-pi/90) 0; sin(-pi/90) cos(-pi/90) 0; 0 0 1];
        taxels_leftArm1(1:3,:)=R*taxels_leftArm1(1:3,:);
        taxels_leftArm3(1:3,:)=R*taxels_leftArm3(1:3,:);
        
        %VISUALISE
        figure();
        scatter3(taxels_leftArm1(1,:),taxels_leftArm1(2,:),taxels_leftArm1(3,:),20,"red");
        hold on;
        scatter3(taxels_leftArm2(1,:),taxels_leftArm2(2,:),taxels_leftArm2(3,:),20,"green");
        scatter3(taxels_leftArm3(1,:),taxels_leftArm3(2,:),taxels_leftArm3(3,:),20,"blue");
        axis equal;
        view(90,0);
    else %rightArm
        diff_183=taxels_rightArm1(1:3,find(taxels_rightArm1(4,:) == 183))-taxels_rightArm2(1:3,find(taxels_rightArm2(4,:) == 183));
        taxels_rightArm2(1:3,:)=taxels_rightArm2(1:3,:)+diff_183;
        taxels_rightArm3(1:3,:)=taxels_rightArm3(1:3,:)-taxels_rightArm1(1:3,find(taxels_rightArm1(4,:) == 183));
        taxels_rightArm1(1:3,:)=taxels_rightArm1(1:3,:)-taxels_rightArm1(1:3,find(taxels_rightArm1(4,:) == 183));
        taxels_rightArm2(1:3,:)=taxels_rightArm2(1:3,:)-taxels_rightArm2(1:3,find(taxels_rightArm2(4,:) == 183));
        
        z=taxels_rightArm2(1:3,find(taxels_rightArm2(4,:) == 189));
        y=taxels_rightArm2(1:3,find(taxels_rightArm2(4,:) == 180));
        x=cross(y,z);
        z=cross(x,y);
        x=x/norm(x);
        y=y/norm(y);
        z=z/norm(z);
        A=[x y z];
        taxels_rightArm2(1:3,:)=inv(A)*taxels_rightArm2(1:3,:);
        
        z=taxels_rightArm1(1:3,find(taxels_rightArm1(4,:) == 189));
        y=taxels_rightArm1(1:3,find(taxels_rightArm1(4,:) == 180));
        x=cross(y,z);
        z=cross(x,y);
        x=x/norm(x);
        y=y/norm(y);
        z=z/norm(z);
        A=[x y z];
        taxels_rightArm1(1:3,:)=inv(A)*taxels_rightArm1(1:3,:);
        taxels_rightArm3(1:3,:)=inv(A)*taxels_rightArm3(1:3,:);
        
        %move origin back to taxel 3
        taxels_rightArm3(1:3,:)=taxels_rightArm3(1:3,:)-taxels_rightArm2(1:3,find(taxels_rightArm2(4,:) == 3));
        taxels_rightArm1(1:3,:)=taxels_rightArm1(1:3,:)-taxels_rightArm2(1:3,find(taxels_rightArm2(4,:) == 3));
        taxels_rightArm2(1:3,:)=taxels_rightArm2(1:3,:)-taxels_rightArm2(1:3,find(taxels_rightArm2(4,:) == 3));
        
        z=taxels_rightArm2(1:3,find(taxels_rightArm2(4,:) == 0));
        y=taxels_rightArm2(1:3,find(taxels_rightArm2(4,:) == 2));
        x=cross(y,z);
        z=cross(x,y);
        x=x/norm(x);
        y=y/norm(y);
        z=z/norm(z);
        A=[x y z];
        taxels_rightArm1(1:3,:)=inv(A)*taxels_rightArm1(1:3,:);
        taxels_rightArm2(1:3,:)=inv(A)*taxels_rightArm2(1:3,:);
        taxels_rightArm3(1:3,:)=inv(A)*taxels_rightArm3(1:3,:);
        
        %VISUALISE
        figure();
        scatter3(taxels_rightArm1(1,:),taxels_rightArm1(2,:),taxels_rightArm1(3,:),20,"red");
        hold on;
        scatter3(taxels_rightArm2(1,:),taxels_rightArm2(2,:),taxels_rightArm2(3,:),20,"green");
        scatter3(taxels_rightArm3(1,:),taxels_rightArm3(2,:),taxels_rightArm3(3,:),20,"blue");
        axis equal;
        view(90,0);
    end
end

%AXIS:
%{
plot3([0,0.01],[0,0],[0,0],'red');
plot3([0,0],[0,0.01],[0,0],'green');
plot3([0,0],[0,0],[0,0.01],'blue');
%}