%Makes pointcloud for calibration by combining the 3 collected datasets
%from 3D reconstruction (each point is taken as mean of the reconstructed positions)

for part={'leftArm'} %'head','torso','rightArm'
    part=part{1};
    for i=1:3
        load(strcat(part,num2str(i),'_scaled_c.mat'));
    end
    
    if(strcmp(part,'head'))
        head=zeros(384,6);
        devs_head=zeros(384,3);
        for i=1:384
            if head3(i,1:3)==[0, 0, 0]
                head(i,1:3)=mean([head1(i,1:3);head2(i,1:3)],1);
                devs_head(i,1)=norm(head1(i,1:3)-head(i,1:3));
                devs_head(i,2)=norm(head2(i,1:3)-head(i,1:3));
            else
                head(i,1:3)=mean([head1(i,1:3);head2(i,1:3);head3(i,1:3)],1);
                devs_head(i,1)=norm(head1(i,1:3)-head(i,1:3));
                devs_head(i,2)=norm(head2(i,1:3)-head(i,1:3));
                devs_head(i,3)=norm(head3(i,1:3)-head(i,1:3));
            end
        end
        
    elseif(strcmp(part,'torso'))
        torso=zeros(384,6);
        devs_torso=zeros(384,3);
        for i=1:384
            if torso2(i,1:3)==[0, 0, 0]
                torso(i,1:3)=mean([torso1(i,1:3);torso3(i,1:3)],1);
                devs_torso(i,1)=norm(torso1(i,1:3)-torso(i,1:3));
                devs_torso(i,3)=norm(torso3(i,1:3)-torso(i,1:3));
            else
                torso(i,1:3)=mean([torso1(i,1:3);torso2(i,1:3);torso3(i,1:3)],1);
                devs_torso(i,1)=norm(torso1(i,1:3)-torso(i,1:3));
                devs_torso(i,2)=norm(torso2(i,1:3)-torso(i,1:3));
                devs_torso(i,3)=norm(torso3(i,1:3)-torso(i,1:3));
            end
        end
        
    elseif(strcmp(part,'leftArm'))
        leftArm=zeros(384,6);
        devs_lA=zeros(384,3);
        for i=1:384
            m=[0 0 0];
            num_dat=[0 0 0];
            if leftArm1(i,1:3)~=[0, 0, 0]
                m=m+leftArm1(i,1:3);
                num_dat(1)=1;
            end
            if leftArm2(i,1:3)~=[0, 0, 0]
                m=m+leftArm2(i,1:3);
                num_dat(2)=1;
            end
            if leftArm3(i,1:3)~=[0, 0, 0]
                m=m+leftArm3(i,1:3);
                num_dat(3)=1;
            end
            if sum(num_dat)~=0
                leftArm(i,1:3)=m/sum(num_dat);
            end
            devs_lA(i,1)=num_dat(1)*norm(leftArm1(i,1:3)-leftArm(i,1:3));
            devs_lA(i,2)=num_dat(2)*norm(leftArm2(i,1:3)-leftArm(i,1:3));
            devs_lA(i,3)=num_dat(3)*norm(leftArm3(i,1:3)-leftArm(i,1:3));
        end
    else %rightArm
        rightArm=zeros(384,6);
        devs_rA=zeros(384,3);
        for i=1:384
            m=[0 0 0];
            num_dat=[0 0 0];
            if rightArm1(i,1:3)~=[0, 0, 0]
                m=m+rightArm1(i,1:3);
                num_dat(1)=1;
            end
            if rightArm2(i,1:3)~=[0, 0, 0]
                m=m+rightArm2(i,1:3);
                num_dat(2)=1;
            end
            if rightArm3(i,1:3)~=[0, 0, 0]
                m=m+rightArm3(i,1:3);
                num_dat(3)=1;
            end
            if sum(num_dat)~=0
                rightArm(i,1:3)=m/sum(num_dat);
            end
            devs_rA(i,1)=num_dat(1)*norm(rightArm1(i,1:3)-rightArm(i,1:3));
            devs_rA(i,2)=num_dat(2)*norm(rightArm2(i,1:3)-rightArm(i,1:3));
            devs_rA(i,3)=num_dat(3)*norm(rightArm3(i,1:3)-rightArm(i,1:3));
        end
    end
end

%visualise deviations:
%{
for i=1:3
    figure();
    %stem(devs_head(:,i)*1000);
    %stem(devs_torso(:,i)*1000);
    %stem(devs_lA(:,i)*1000);
    stem(devs_rA(:,i)*1000);
    hold on;
    title(strcat('Taxel deviations of dataset ',num2str(i),' - right arm'));
    xlabel('taxel');
    ylabel('deviation [mm]');
end
%}