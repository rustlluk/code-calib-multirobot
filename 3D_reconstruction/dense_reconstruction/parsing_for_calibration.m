for part={'leftArm','rightArm'} %,'head','torso'
    part=part{1};
    for i=1:3
        load(strcat(part,num2str(i),'_scaled.mat'));
    end
    
    if(strcmp(part,'head'))
        head1=zeros(384,6);
        head2=zeros(384,6);
        head3=zeros(384,6);
        for i=1:size(taxels_head1,2)
            head1(taxels_head1(4,i)+1,1:3)=taxels_head1(1:3,i);
        end
        for i=1:size(taxels_head2,2)
            head2(taxels_head2(4,i)+1,1:3)=taxels_head2(1:3,i);
        end
        for i=1:size(taxels_head3,2)
            head3(taxels_head3(4,i)+1,1:3)=taxels_head3(1:3,i);
        end
    elseif(strcmp(part,'torso'))
        torso1=zeros(384,6);
        torso2=zeros(384,6);
        torso3=zeros(384,6);
        for i=1:size(taxels_torso1,2)
            torso1(taxels_torso1(4,i)+1,1:3)=taxels_torso1(1:3,i);
        end
        for i=1:size(taxels_torso2,2)
            torso2(taxels_torso2(4,i)+1,1:3)=taxels_torso2(1:3,i);
        end
        for i=1:size(taxels_torso3,2)
            torso3(taxels_torso3(4,i)+1,1:3)=taxels_torso3(1:3,i);
        end
    elseif(strcmp(part,'leftArm'))
        leftArm1=zeros(384,6);
        leftArm2=zeros(384,6);
        leftArm3=zeros(384,6);
        for i=1:size(taxels_leftArm1,2)
            leftArm1(taxels_leftArm1(4,i)+1,1:3)=taxels_leftArm1(1:3,i);
        end
        for i=1:size(taxels_leftArm2,2)
            leftArm2(taxels_leftArm2(4,i)+1,1:3)=taxels_leftArm2(1:3,i);
        end
        for i=1:size(taxels_leftArm3,2)
            leftArm3(taxels_leftArm3(4,i)+1,1:3)=taxels_leftArm3(1:3,i);
        end
    else %rightArm
        rightArm1=zeros(384,6);
        rightArm2=zeros(384,6);
        rightArm3=zeros(384,6);
        for i=1:size(taxels_rightArm1,2)
            rightArm1(taxels_rightArm1(4,i)+1,1:3)=taxels_rightArm1(1:3,i);
        end
        for i=1:size(taxels_rightArm2,2)
            rightArm2(taxels_rightArm2(4,i)+1,1:3)=taxels_rightArm2(1:3,i);
        end
        for i=1:size(taxels_rightArm3,2)
            rightArm3(taxels_rightArm3(4,i)+1,1:3)=taxels_rightArm3(1:3,i);
        end
    end
end
