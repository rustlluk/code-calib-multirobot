%EDGES=0; %show triangle edges (used only for Colmap models)
%CENTERS=0; %show triangle centers (used only for Colmap models)
TAXELS=1; %show taxels
SEPARATE_PARTS=1; %divide parts into different figures
IDS=1; %show taxel IDs

%k=0.198860555092273; %scaling coefficient computed from the mean of edges
%of all triangles (used for Colmap models)

for part={'leftArm','rightArm'}
    part=part{1}; %'torso','head',
    if(SEPARATE_PARTS)
        fig=figure('Name', part);
    else
        hold on;
    end
    %{
    if(EDGES)
        [xyz,~]=read_meshlab_log('head_corners1.txt');
        xyz=diag([k,k,k])*xyz;
        plot3(xyz(1,:),xyz(2,:),xyz(3,:),'.k');
        for i=1:size(xyz,2)/3
            plot3([xyz(1,3*i-2),xyz(1,3*i-1),xyz(1,3*i),xyz(1,3*i-2)],[xyz(2,3*i-2),xyz(2,3*i-1),xyz(2,3*i),xyz(2,3*i-2)],[xyz(3,3*i-2),xyz(3,3*i-1),xyz(3,3*i),xyz(3,3*i-2)])
            hold on;
        end
    end
    if(CENTERS)
        [xyz,~]=read_meshlab_log('head_corners1.txt');
        xyz=diag([k,k,k])*xyz;
        centers=centers_from_corners(xyz);
        centers==diag([k,k,k])*centers;
        scatter3(centers(1,:),centers(2,:),centers(3,:),"Marker","x","MarkerEdgeColor","red");
    end
    %}
    if(TAXELS)
        %[taxels,~]=read_meshlab_log('m_head2');
        taxels=read_picked_points(strcat('picked_points_',part,'.pp'));
        %taxels=diag([k,k,k])*taxels;
        scatter3(taxels(1,:),taxels(2,:),taxels(3,:),20,"black");
        if(IDS)
            text(taxels(1,:),taxels(2,:),taxels(3,:),num2str(taxels(4,:)'),'VerticalAlignment','top','HorizontalAlignment', 'center');
        end
    end
    axis equal;
    view(0,-90);
end

%visualise the scaled models
%{
for i=1:3
    load(strcat('torso',num2str(i),'_scaled'));
end
scatter3(taxels_torso1(1,:),taxels_torso1(2,:),taxels_torso1(3,:),20,"red");
hold on;
scatter3(taxels_torso2(1,:),taxels_torso2(2,:),taxels_torso2(3,:),20,"green");
scatter3(taxels_torso3(1,:),taxels_torso3(2,:),taxels_torso3(3,:),20,"blue");
axis equal;
view(90,0);
%}