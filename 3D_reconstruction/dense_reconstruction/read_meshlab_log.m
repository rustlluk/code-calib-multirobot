function [xyz,normals] = read_meshlab_log(path)
    % Extracts vertices coordinates from meshlab log
    xyz=[[0;0;0]];
    normals=[[0;0;0]];
    num_elems=0;
    file = fopen(path, 'r');
    line=fgetl(file);
    while(ischar(line))
        if(contains(line,'vertex#'))
            line=fgetl(file);
            pos=sscanf(line,strcat("position [","%f"," ","%f"," ","%f"));
            line=fgetl(file);
            norm=sscanf(line,strcat("normal [","%f"," ","%f"," ","%f"));
            fgetl(file);
            fgetl(file);
            num_elems=num_elems+1;
            xyz(:,num_elems)=pos;
            normals(:,num_elems)=norm;
        end
        line=fgetl(file);
    end
    fclose(file);
end