function points = read_picked_points(path)
    %READ_PICKED_POINTS Summary of this function goes here
    %   Detailed explanation goes here
    xyz=[[0;0;0]];
    id=[0];
    num_elems=0;
    file = fopen(path, 'r');
    line=fgetl(file);
    while(~contains(line,'/DocumentData'))
        line=fgetl(file);
    end
    line=fgetl(file);
    while(~contains(line,'/PickedPoints'))
        num_elems=num_elems+1;
        xyz(1,num_elems)=sscanf(line(strfind(line,'x'):end),'x="%f');
        xyz(2,num_elems)=sscanf(line(strfind(line,'y'):end),'y="%f');
        xyz(3,num_elems)=sscanf(line(strfind(line,'z'):end),'z="%f');
        id(num_elems)=sscanf(line(strfind(line,'name'):end),'name="%f');
        line=fgetl(file);
    end
    points=[xyz;id];
    fclose(file);
end

