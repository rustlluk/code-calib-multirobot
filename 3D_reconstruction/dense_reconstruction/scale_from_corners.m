function [k, M, deviations] =scale_from_corners(xyz)
    %computes the scaling coefficient from coordinates of the triangles'
    %corners, assuming the real edge size is 3cm
    %return also mean edge length and deviations
    distances=zeros(3,1);
    for i=1:size(xyz,2)/3
        distances(3*i-2)=norm(xyz(:,3*i-2)-xyz(:,3*i-1));
        distances(3*i-1)=norm(xyz(:,3*i-1)-xyz(:,3*i));
        distances(3*i)=norm(xyz(:,3*i)-xyz(:,3*i-2));
    end
    M=mean(distances);
    k=0.03/M;
    deviations=k*(distances-M);
end