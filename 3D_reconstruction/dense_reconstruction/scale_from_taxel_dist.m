function [k,M,deviations] = scale_from_taxel_dist(dataset_num)
    %SCALE_FROM_TAXEL_DIST Summary of this function goes here
    %   Detailed explanation goes here
    distances=[0];
    if(dataset_num==2)
        part='head';
    else
        part='torso';
    end
    load(strcat(part,num2str(dataset_num),'.mat'));
    switch dataset_num
        case 1
            for i=1:25
                distances((i-1)*5+1)=norm(taxels_torso1(1:3,(i-1)*10+2)-taxels_torso1(1:3,(i-1)*10+1));
                distances((i-1)*5+2)=norm(taxels_torso1(1:3,(i-1)*10+3)-taxels_torso1(1:3,(i-1)*10+2));
                distances((i-1)*5+3)=norm(taxels_torso1(1:3,(i-1)*10+6)-taxels_torso1(1:3,(i-1)*10+5));
                distances((i-1)*5+4)=norm(taxels_torso1(1:3,(i-1)*10+9)-taxels_torso1(1:3,(i-1)*10+8));
                distances((i-1)*5+5)=norm(taxels_torso1(1:3,(i-1)*10+10)-taxels_torso1(1:3,(i-1)*10+9));
            end
        case 2
            for i=1:24
                distances((i-1)*5+1)=norm(taxels_head2(1:3,(i-1)*10+2)-taxels_head2(1:3,(i-1)*10+1));
                distances((i-1)*5+2)=norm(taxels_head2(1:3,(i-1)*10+3)-taxels_head2(1:3,(i-1)*10+2));
                distances((i-1)*5+3)=norm(taxels_head2(1:3,(i-1)*10+6)-taxels_head2(1:3,(i-1)*10+5));
                distances((i-1)*5+4)=norm(taxels_head2(1:3,(i-1)*10+9)-taxels_head2(1:3,(i-1)*10+8));
                distances((i-1)*5+5)=norm(taxels_head2(1:3,(i-1)*10+10)-taxels_head2(1:3,(i-1)*10+9));
            end
        case 3
            for i=1:25
                distances((i-1)*5+1)=norm(taxels_torso3(1:3,(i-1)*10+2)-taxels_torso3(1:3,(i-1)*10+1));
                distances((i-1)*5+2)=norm(taxels_torso3(1:3,(i-1)*10+3)-taxels_torso3(1:3,(i-1)*10+2));
                distances((i-1)*5+3)=norm(taxels_torso3(1:3,(i-1)*10+6)-taxels_torso3(1:3,(i-1)*10+5));
                distances((i-1)*5+4)=norm(taxels_torso3(1:3,(i-1)*10+9)-taxels_torso3(1:3,(i-1)*10+8));
                distances((i-1)*5+5)=norm(taxels_torso3(1:3,(i-1)*10+10)-taxels_torso3(1:3,(i-1)*10+9));
            end
    end
    M=mean(distances);
    k=0.0065/M;
    deviations=k*(distances-M);
end

