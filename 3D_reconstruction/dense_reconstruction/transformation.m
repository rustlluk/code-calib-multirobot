part='rightArm';

if(strcmp(part,'head'))
    pts=read_picked_points('picked_points_camera_circuit.pp');
    taxels_head1=read_picked_points('picked_points_head.pp');
    center=mean(pts(1:3,:),2);
    pts(1:3,:)=pts(1:3,:)-center;
    z=taxels_head1(1:3,find(taxels_head1(4,:) == 189))-taxels_head1(1:3,find(taxels_head1(4,:) == 0));
    y=taxels_head1(1:3,find(taxels_head1(4,:) == 69))-taxels_head1(1:3,find(taxels_head1(4,:) == 229));
elseif(strcmp(part,'torso'))
    pts=read_picked_points('picked_points_button_circuit.pp'); %get pts from button circuit
    taxels_torso1=read_picked_points('picked_points_torso.pp');
    %pts=taxels_torso2(:,find(ismember(taxels_torso2(4,:),[65;129;133;169;357;289]))); %get pts from taxels around the button
    center=mean(pts(1:3,:),2); %65 129 133 169 357 289
    pts(1:3,:)=pts(1:3,:)-center;
    z=taxels_torso1(1:3,find(taxels_torso1(4,:) == 63))-taxels_torso1(1:3,find(taxels_torso1(4,:) == 189));
    y=taxels_torso1(1:3,find(taxels_torso1(4,:) == 165))-taxels_torso1(1:3,find(taxels_torso1(4,:) == 325));
elseif(strcmp(part,'leftArm'))
    taxels_leftArm3=read_picked_points('picked_points_leftArm3.pp');
    center=taxels_leftArm3(1:3,find(taxels_leftArm3(4,:) == 255)); %3, 3, 255
    taxels_leftArm3(1:3,:)=taxels_leftArm3(1:3,:)-center;
    z=taxels_leftArm3(1:3,find(taxels_leftArm3(4,:) == 263)); %0, 263
    y=taxels_leftArm3(1:3,find(taxels_leftArm3(4,:) == 253)); %2, 253
else %rightArm
    taxels_rightArm3=read_picked_points('picked_points_rightArm3.pp');
    center=taxels_rightArm3(1:3,find(taxels_rightArm3(4,:) == 255)); %255, 3, 255
    taxels_rightArm3(1:3,:)=taxels_rightArm3(1:3,:)-center;
    z=taxels_rightArm3(1:3,find(taxels_rightArm3(4,:) == 263)); %0, 263
    y=taxels_rightArm3(1:3,find(taxels_rightArm3(4,:) == 253)); %2, 253
end

%{
scatter3(pts(1,:),pts(2,:),pts(3,:),20,"black");
text(pts(1,:),pts(2,:),pts(3,:),num2str(pts(4,:)'),'VerticalAlignment','top','HorizontalAlignment', 'center');
axis equal;
%}

x=cross(y,z);
z=cross(x,y);
x=x/norm(x);
y=y/norm(y);
z=z/norm(z);
A=[x y z];

%{
plot3([0,x(1)],[0,x(2)],[0,x(3)]);
hold on;
plot3([0,y(1)],[0,y(2)],[0,y(3)]);
plot3([0,z(1)],[0,z(2)],[0,z(3)]);
%}
%{
pts(1:3,:)=A*pts(1:3,:);
scatter3(pts(1,:),pts(2,:),pts(3,:),20,"black");
text(pts(1,:),pts(2,:),pts(3,:),num2str(pts(4,:)'),'VerticalAlignment','top','HorizontalAlignment', 'center');
axis equal;
%}

if(strcmp(part,'head'))
    taxels_head1(1:3,:)=taxels_head1(1:3,:)-center;
    taxels_head1(1:3,:)=inv(A)*taxels_head1(1:3,:);
    
    scatter3(taxels_head1(1,:),taxels_head1(2,:),taxels_head1(3,:),20,"black");
    text(taxels_head1(1,:),taxels_head1(2,:),taxels_head1(3,:),num2str(taxels_head1(4,:)'),'VerticalAlignment','top','HorizontalAlignment', 'center');
    axis equal;
    view(90,0);
elseif(strcmp(part,'torso'))
    taxels_torso1(1:3,:)=taxels_torso1(1:3,:)-center;
    taxels_torso1(1:3,:)=inv(A)*taxels_torso1(1:3,:);
    
    scatter3(taxels_torso1(1,:),taxels_torso1(2,:),taxels_torso1(3,:),20,"black");
    text(taxels_torso1(1,:),taxels_torso1(2,:),taxels_torso1(3,:),num2str(taxels_torso1(4,:)'),'VerticalAlignment','top','HorizontalAlignment', 'center');
    axis equal;
    view(90,0);
elseif(strcmp(part,'leftArm'))
    taxels_leftArm3(1:3,:)=inv(A)*taxels_leftArm3(1:3,:);
    
    scatter3(taxels_leftArm3(1,:),taxels_leftArm3(2,:),taxels_leftArm3(3,:),20,"black");
    text(taxels_leftArm3(1,:),taxels_leftArm3(2,:),taxels_leftArm3(3,:),num2str(taxels_leftArm3(4,:)'),'VerticalAlignment','top','HorizontalAlignment', 'center');
    axis equal;
    view(90,0);
else
    taxels_rightArm3(1:3,:)=inv(A)*taxels_rightArm3(1:3,:);
    
    scatter3(taxels_rightArm3(1,:),taxels_rightArm3(2,:),taxels_rightArm3(3,:),20,"black");
    text(taxels_rightArm3(1,:),taxels_rightArm3(2,:),taxels_rightArm3(3,:),num2str(taxels_rightArm3(4,:)'),'VerticalAlignment','top','HorizontalAlignment', 'center');
    axis equal;
    view(90,0);
end