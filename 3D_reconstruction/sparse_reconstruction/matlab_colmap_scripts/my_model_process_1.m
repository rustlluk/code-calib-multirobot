[cam,img,pts]=read_model('colmap_sparse_models/session1');
%plot_model(cam,img,pts);

keySet=keys(pts);
valueSet=values(pts);

%reducing to bench with robot

for i=1:pts.length
    if(valueSet{i}.xyz(1)<-9 || valueSet{i}.xyz(1)>1 || valueSet{i}.xyz(2)<1 || valueSet{i}.xyz(2)>4 || valueSet{i}.xyz(3)<-4 || valueSet{i}.xyz(3)>5)
        remove(pts,keySet{i});
    end
end
keySet=keys(pts);
valueSet=values(pts);

%rotating
phi=5*pi/4-0.1222;
Ry=[cos(phi) 0 sin(phi); 0 1 0; -sin(phi) 0 cos(phi)];
phi=0.0524;
Rx=[1 0 0; 0 cos(phi) -sin(phi); 0 sin(phi) cos(phi)];
phi=pi;
Rz=[cos(phi) -sin(phi) 0; sin(phi) cos(phi) 0; 0 0 1];
R=Rz*Rx*Ry;
for i=1:pts.length
    valueSet{i}.xyz=R*valueSet{i}.xyz;
    pts(keySet{i})=valueSet{i};
end
%}
%plot_model(containers.Map,containers.Map,pts);

%head cutout
%{
for i=1:pts.length
    if(valueSet{i}.xyz(1)<-1 || valueSet{i}.xyz(1)>0.2 || valueSet{i}.xyz(2)<-1.82 || valueSet{i}.xyz(2)>-1.2 || valueSet{i}.xyz(3)<-4.2 || valueSet{i}.xyz(3)>-3.2)
        remove(pts,keySet{i});
    end
end
keySet=keys(pts);
valueSet=values(pts);
head_pts_array=zeros(3,pts.length);
for i = 1:pts.length
    point = pts(keySet{i});
    head_pts_array(:,i) = point.xyz;
end
%}

%torso cutout
%{
for i=1:pts.length
    if(valueSet{i}.xyz(1)<-0.8 || valueSet{i}.xyz(1)>0 || valueSet{i}.xyz(2)<-2.6 || valueSet{i}.xyz(2)>-1.8 || valueSet{i}.xyz(3)<-4 || valueSet{i}.xyz(3)>-3)
        remove(pts,keySet{i});
    end
end
keySet=keys(pts);
valueSet=values(pts);
torso_pts_array=zeros(3,pts.length);
for i = 1:pts.length
    point = pts(keySet{i});
    torso_pts_array(:,i) = point.xyz;
end
%}

%left hand cutout
%
for i=1:pts.length
    if(valueSet{i}.xyz(1)<0 || valueSet{i}.xyz(1)>0.7 || valueSet{i}.xyz(2)<-3.3 || valueSet{i}.xyz(2)>-2.4 || valueSet{i}.xyz(3)<-4 || valueSet{i}.xyz(3)>-3)
        remove(pts,keySet{i});
    end
end
keySet=keys(pts);
valueSet=values(pts);
left_hand_pts_array=zeros(3,pts.length);
for i = 1:pts.length
    point = pts(keySet{i});
    left_hand_pts_array(:,i) = point.xyz;
end
%}

%right hand cutout
%{
for i=1:pts.length
    if(valueSet{i}.xyz(1)<-1.3 || valueSet{i}.xyz(1)>-0.7 || valueSet{i}.xyz(2)<-3.3 || valueSet{i}.xyz(2)>-2.5 || valueSet{i}.xyz(3)<-4 || valueSet{i}.xyz(3)>-3)
        remove(pts,keySet{i});
    end
end
keySet=keys(pts);
valueSet=values(pts);
right_hand_pts_array=zeros(3,pts.length);
for i = 1:pts.length
    point = pts(keySet{i});
    right_hand_pts_array(:,i) = point.xyz;
end
%}