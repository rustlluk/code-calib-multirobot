%{
%finger calibration
robot_fcn = 'loadNAO'; % Name of the function with robot structure
config_fcn = 'optimizationConfig'; % Name of the function with calibration config
approaches = {'selftouch'}; % Used approaches, delimited by comma (,)
chains = {'leftFinger'}; % Used chains, delimited by comma (,) 
jointTypes={'joint'}; % Used body parts, delimited by comma (,), in 'motomanOptConfig' joint is set by default
dataset_fcn = 'loadDatasetNao'; % Name of the function for loading of the dataset
whitelist_fcn = ''; % Name of the function with custom whitelist
bounds_fcn = ''; % Name of the funtion with custom bounds
dataset_params = {'torso_leftFinger','rightArm_leftFinger'}; % Params of the 'dataset_fcn', delimited by comma (,)
folder = 'both_F_calibrated'; % Folder where results will be saved (relatively to 'results' folder)
saveInfo = [1, 1, 1]; % 1/0, determines whether to save results
loadDHfunc = 'loadDHfromMat'; % name of function to load DH ('loadDHfromMat','loadDHfromTxt')
loadDHargs = {}; % arguments for the function above
loadDHfolder = 'both_F_calibrated'; % folder from which to load DH
%}
%
robot_fcn = 'loadNAO'; % Name of the function with robot structure
config_fcn = 'optimizationConfig'; % Name of the function with calibration config
approaches = {'selftouch'}; % Used approaches, delimited by comma (,)
chains = {'torso'}; % Used chains, delimited by comma (,) 
jointTypes={'triangle'}; % Used body parts, delimited by comma (,), in 'motomanOptConfig' joint is set by default
dataset_fcn = 'loadDatasetNao'; % Name of the function for loading of the dataset
whitelist_fcn = ''; % Name of the function with custom whitelist
bounds_fcn = ''; % Name of the funtion with custom bounds
dataset_params = {'torso_rightFinger'}; % Params of the 'dataset_fcn', delimited by comma (,)
folder = 't_rF_triangle'; % Folder where results will be saved (relatively to 'results' folder)
saveInfo = [1, 1, 1]; % 1/0, determines whether to save results
loadDHfunc = 'loadDHfromMat'; % name of function to load DH ('loadDHfromMat','loadDHfromTxt')
loadDHargs = {}; % arguments for the function above
loadDHfolder = 't_rF_triangle'; % folder from which to load DH
%}

runCalibration(robot_fcn, config_fcn, approaches, chains, jointTypes, dataset_fcn, whitelist_fcn, bounds_fcn, dataset_params, folder, saveInfo, loadDHfunc, loadDHargs, loadDHfolder);
%runCalibration(robot_fcn, config_fcn, approaches, chains, jointTypes, dataset_fcn, whitelist_fcn, bounds_fcn, dataset_params, folder, saveInfo, loadDHfunc, loadDHargs, loadDHfolder);
%runCalibration(robot_fcn, config_fcn, approaches, chains, jointTypes, dataset_fcn, whitelist_fcn, bounds_fcn, dataset_params, folder, saveInfo, loadDHfunc, loadDHargs, loadDHfolder);

%
load(strcat('Results/',folder, '/info'));
load(strcat('Results/',folder, '/results.mat'));
loadDHfromMat(rob, folder, 'type' ,'min');
[~,~,datasets] = rob.prepareDataset(optim, chains, dataset_fcn,dataset_params);
activationsView(rob,{datasets.selftouch{:}},'info',1,'skin',1,'finger',1)
getTaxelDistances(rob,chains,dataset_params)
%}